import os
import signal
import asyncio
import functools
from concurrent.futures import ThreadPoolExecutor
from functools import partial
from multiprocessing import Process
from time import time
from enum import IntEnum
from typing import cast

from logger.logger import create_logger
from config.config import Config

logger = create_logger(__file__, False)

config = Config()

__all__ = 'watch', 'FileWatcher', 'run_process', 'arun_process'


class FilesChangeType(IntEnum):
    """
    Files changes types
    """
    added = 1
    modified = 2
    deleted = 3


def watch(path, **kwargs):
    """
    Watch for files in directories
    :param path: path to directory
    :param kwargs: optional fields (for future work)
    :return:
    """
    main_thread_loop = asyncio.new_event_loop()

    try:
        worker_async_watch = FileWatcher(path,
                                         loop=main_thread_loop,
                                         **kwargs)
        while True:
            try:
                yield main_thread_loop.run_until_complete(worker_async_watch.__anext__())
            except StopAsyncIteration as sai:
                logger.error(f'Stop Async Iteration exception: {sai}')

    except KeyboardInterrupt:
        logger.debug('Keyboard Interrupt, exiting...')

    finally:
        main_thread_loop.close()


class BaseFileWatcher:
    """
    Base class for Broker file watcher
    """

    def __init__(self,
                 root_path,
                 ignored_paths=None):
        self.files = dict()
        self._root_path = str(root_path)
        self._ignored_paths = ignored_paths
        self._ignored_dirs = config.watcher.ignored_dirs

        self.check_on_fly()

    def _should_watch_this_dir(self, entry):
        """
        Redefined entry: check if this directory should be analyzed
        :param entry: directory path
        :return:
        """
        return True

    def _should_watch_this_file(self, entry):
        """
        Redefined entry: check if this file should be analyzed
        :param entry: file path
        :return:
        """
        return True

    def _walk_through(self,
                      path,
                      changes,
                      new_files):
        """
        Go through watch list
        :param path: path to analyze
        :param changes: mutable list of changes (tuples)
        :param new_files: mutable dict of new files in directory
        :return:
        """
        if os.path.isfile(path):
            self._watch_the_file(path,
                                 changes,
                                 new_files,
                                 os.stat(path))
        else:
            self._walk_through_directory(path,
                                         changes,
                                         new_files)

    def _watch_the_file(self,
                        path,
                        changes,
                        new_files,
                        stat):
        """
        Watch this file on changes
        :param path: path to file
        :param changes: mutable list of changes (tuples)
        :param new_files: mutable dict of new files in directory of this file
        :param stat: file stats from OS package
        :return:
        """
        mtime = stat.st_mtime

        new_files[path] = mtime

        old_mtime = self.files.get(path)

        if not old_mtime:
            changes.add((FilesChangeType.added, path))
        elif old_mtime != mtime:
            changes.add((FilesChangeType.modified, path))

    def _walk_through_directory(self,
                                dir_path,
                                changes,
                                new_files):
        """
        Watch this directory on changes
        :param dir_path: path to directory
        :param changes: mutable list of changes (tuples)
        :param new_files: mutable dict of new files in directory
        :return:
        """
        for entry in os.scandir(dir_path):
            if self._ignored_paths is not None:
                if os.path.join(dir_path, entry) in self._ignored_paths:
                    continue

            if entry.is_dir():
                if self._should_watch_this_dir(entry):
                    self._walk_through_directory(entry.path,
                                                 changes,
                                                 new_files)
            elif self._should_watch_this_file(entry):
                self._watch_the_file(entry.path,
                                     changes,
                                     new_files,
                                     entry.stat())

    def check_on_fly(self):
        """
        Initial point of path scan
        :return:
        """
        changes = set()
        new_files = dict()

        try:
            self._walk_through(self._root_path,
                               changes,
                               new_files)
        except OSError as e:
            logger.warning(f'Error scan file system: {e.__class__.__name__} {e}')

        deleted = self.files.keys() - new_files.keys()

        if deleted:
            changes |= {(FilesChangeType.deleted, entry) for entry in deleted}

        self.files = new_files

        return changes


class PythonWatcher(BaseFileWatcher):
    """
    Python files watcher
    """

    def _should_watch_this_dir(self, entry):
        """
        Redefined entry: check if this directory should be analyzed
        :param entry: directory path
        :return:
        """
        return entry.name not in self._ignored_dirs

    def _should_watch_this_file(self, entry):
        """
        Redefined entry: check if this file should be analyzed
        :param entry: file path
        :return:
        """
        return entry.name.endswith(Config().watcher.file_endwith)


class FileWatcher:
    """
    Broker File Watcher
    """

    def __init__(self,
                 path,
                 *,
                 watcher_cls=PythonWatcher,
                 watcher_kwargs=None,
                 debounce=Config().watcher.debounce,
                 normal_sleep=Config().watcher.normal_sleep,
                 min_sleep=Config().watcher.min_sleep,
                 stop_event=None,
                 loop=None):
        self._loop = loop or asyncio.get_event_loop()
        self._executor = ThreadPoolExecutor(max_workers=Config().watcher.max_workers)
        self._path = path
        self._watcher_cls = watcher_cls
        self._watcher_kwargs = watcher_kwargs or dict()
        self._debounce = debounce
        self._normal_sleep = normal_sleep
        self._min_sleep = min_sleep
        self._stop_event = stop_event
        self._w = None
        self._lock = asyncio.Lock()
        asyncio.set_event_loop(self._loop)

    @staticmethod
    def unix_ms():
        """
        Return formatted time
        :return:
        """
        return int(round(time() * 1000))

    def __aiter__(self):
        """
        Return asynchronous iterator
        :return:
        """
        return self

    async def __anext__(self):
        """
        Return an awaitable object
        :return:
        """
        if self._w:
            watcher = self._w
        else:
            watcher = self._w = await self.run_in_executor(
                functools.partial(self._watcher_cls, self._path, **self._watcher_kwargs)
            )

        check_time = 0
        changes = set()
        last_change = 0

        while True:
            if self._stop_event and self._stop_event.is_set():
                raise StopAsyncIteration()

            async with self._lock:
                if not changes:
                    last_change = self.unix_ms()

                if check_time:
                    if changes:
                        sleep_time = self._min_sleep
                    else:
                        sleep_time = max(self._normal_sleep - check_time,
                                         self._min_sleep)

                    await asyncio.sleep(sleep_time / 1000)

                s = self.unix_ms()

                new_changes = await self.run_in_executor(watcher.check_on_fly)

                changes.update(new_changes)

                now = self.unix_ms()
                check_time = now - s
                debounced = now - last_change

                if changes and (not new_changes or debounced > self._debounce):
                    return changes

    async def run_in_executor(self,
                              func,
                              *args):
        return await self._loop.run_in_executor(self._executor,
                                                func,
                                                *args)

    def __del__(self) -> None:
        self._executor.shutdown()


def _start_process(target, args, kwargs):
    """
    Start async Process
    :param target:
    :param args:
    :param kwargs:
    :return:
    """
    process = Process(target=target,
                      args=args,
                      kwargs=kwargs or {})
    process.start()
    return process


def _stop_process(process: Process):
    """
    Stop async Process
    :param process:
    :return:
    """
    if process.is_alive():
        logger.debug(f'Stopping watcher process...')
        pid = cast(int, process.pid)
        os.kill(pid, signal.SIGINT)
        process.join(5)

        if process.exitcode is None:
            logger.warning(f'Process has not terminated, sending SIGKILL')
            os.kill(pid, signal.SIGKILL)
            process.join(1)
        else:
            logger.debug(f'Process stopped')

    else:
        logger.warning(f'Process already dead, exit code: {process.exitcode}')


def run_process(path,
                target,
                *,
                args=(),
                kwargs=None,
                callback=None,
                watcher_cls=PythonWatcher,
                watcher_kwargs=None,
                debounce=300,
                min_sleep=50):
    """
    Run Process with Watcher
    :param path: path to directory
    :param target: target to run
    :param args: arguments
    :param kwargs: key-value arguments
    :param callback: callback function
    :param watcher_cls: class of BrokerWatcher
    :param watcher_kwargs: key-value arguments for BrokerWatcher
    :param debounce: Debounce (ignoring) value
    :param min_sleep: Sleep time
    :return:
    """
    process = _start_process(target=target,
                             args=args,
                             kwargs=kwargs)
    reloads = 0

    try:
        for changes in watch(path,
                             watcher_cls=watcher_cls,
                             debounce=debounce,
                             min_sleep=min_sleep,
                             watcher_kwargs=watcher_kwargs):
            callback and callback(changes)
            _stop_process(process)
            process = _start_process(target=target,
                                     args=args,
                                     kwargs=kwargs)
            reloads += 1
    except Exception as ex:
        logger.error(f'{ex}')
    finally:
        _stop_process(process)

    return reloads


async def arun_process(path,
                       target,
                       *,
                       args=(),
                       kwargs=None,
                       callback=None,
                       watcher_cls=PythonWatcher,
                       debounce=300,
                       min_sleep=500):
    """
    Run Process Async with Watcher
    :param path: path to directory
    :param target: target to run
    :param args: arguments
    :param kwargs: key-value arguments
    :param callback: callback function
    :param watcher_cls: class of BrokerWatcher
    :param debounce: Debounce (ignoring) value
    :param min_sleep: Sleep time
    :return:
    """
    watcher = FileWatcher(path,
                          watcher_cls=watcher_cls,
                          debounce=debounce,
                          min_sleep=min_sleep)
    start_process = partial(_start_process,
                            target=target,
                            args=args,
                            kwargs=kwargs)
    process = await watcher.run_in_executor(start_process)

    reloads = 0

    async for changes in watcher:
        callback and await callback(changes)
        await watcher.run_in_executor(_stop_process, process)
        process = await watcher.run_in_executor(start_process)
        reloads += 1

    await watcher.run_in_executor(_stop_process, process)

    return reloads
