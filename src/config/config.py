
class WatcherConfig:
    ignored_dirs = {
        '.git',
        '__pycache__',
        'site-packages',
        '.idea',
        'node_modules'
    }
    file_endwith = ('.py', '.pyx', '.pyd')
    ignored_name_patterns = ('__init__.py')
    debounce = 1500
    normal_sleep = 300
    min_sleep = 50
    max_workers = 4


class Config:

    def __init__(self):
        self.watcher = WatcherConfig()
        self.watch_dir = './workers'
        self.workers_up_name = 'workers.'
