import asyncio


async def main():
    print(f'Dummy asyncio start tick')
    await asyncio.sleep(3)
    print(f'Dummy asyncio stop tick')


if __name__ == '__main__':
    asyncio.run(main())