import logging.config
import os
from pathlib import Path

BASE_DIR = Path(__file__).parent.parent
LOG_DIR = f'{BASE_DIR}/logs'

if not os.path.isdir(LOG_DIR):
    os.mkdir(LOG_DIR)

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'standard': {
            'format': '[%(asctime)s] %(levelname)-8s: %(name)-20s: %(funcName)-40s: %(message)s',
            'datefmt': '%d.%m.%Y %H:%M:%S',
        },
        'file_format': {
            'format': '[%(asctime)s] %(levelname)-8s;%(name)-20s;%(funcName)-40s;%(message)s',
            'datefmt': '%d.%m.%Y %H:%M:%S',
        },
    },
    'handlers': {
        'stdout': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'standard',
        },
    },
    'loggers': {
        '': {
            'handlers': ['stdout'],
            'level': 'DEBUG',
            'propagate': False,
        },
        'PIL': {
            'handlers': ['stdout'],
            'level': 'INFO',
            'propagate': False,
        },
        'asyncio': {
            'handlers': ['stdout'],
            'level': 'INFO',
            'propagate': False,
        },
        'aiohttp': {
            'handlers': ['stdout'],
            'level': 'WARNING',
            'propagate': False,
        },
    }
}

logging.config.dictConfig(LOGGING)


def create_logger_name(filename: str) -> str:
    return Path(filename).stem


def create_logger(path_file: str, separate_file: bool = True):
    logger_name = create_logger_name(path_file)
    if separate_file:
        log_filename = f'{LOG_DIR}/{logger_name}.log'
        file_handlers = f'file_{logger_name}'
        LOGGING['handlers'][file_handlers] = {
            'level': 'INFO',
            'class': 'logging.FileHandler',
            'formatter': 'file_format',
            'filename': log_filename,
            'mode': 'a+',
            'encoding': 'utf8'
        }

        LOGGING['loggers'][logger_name] = {
            'handlers': ['stdout', file_handlers],
            'level': 'DEBUG',
            'propagate': False,
        }
    logging.config.dictConfig(LOGGING)
    return logging.getLogger(logger_name)