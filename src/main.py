import os
import sys
import asyncio
import types
import importlib
from watcher.watcher import FileWatcher, run_process

from config.config import Config

from logger.logger import create_logger

logger = create_logger(__file__, False)


def dynamic_import(module):
    """
    Dynamic import module
    :param module: module
    :return:
    """
    return importlib.import_module(module)


def check_module(module_name):
    """
    Check if module is available to import
    :param module_name: module name
    :return: None or module spec
    """
    module_spec = importlib.util.find_spec(module_name)
    if module_spec is None:
        logger.error('Module: {} not found'.format(module_name))
        return None
    else:
        logger.info('Module: {} can be imported!'.format(module_name))
        return module_spec


def import_module_from_spec(module_spec):
    """
    Import module from Spec
    :param module_spec: module spec
    :return:
    """
    module = importlib.util.module_from_spec(module_spec)
    module_spec.loader.exec_module(module)
    return module


def import_source(module_name):
    """
    Import module from source code
    :param module_name: module name
    :return:
    """
    module_file_path = module_name.__file__
    module_name = module_name.__name__

    module_spec = importlib.util.spec_from_file_location(module_name,
                                                         module_file_path)

    module = importlib.util.module_from_spec(module_spec)
    module_spec.loader.exec_module(module)

    logger.info(f'The {module_name} module has the following methods: {dir(module)}')


def scan_workers_folder(dir_path=Config().watch_dir):
    """
    Scan Workers folder for executable files
    :param dir_path: path to workers folder
    :return: list of file paths
    """
    files = []
    for entry in os.scandir(dir_path):
        if entry.is_file() and entry.path.endswith('.py') and entry.name not in Config().watcher.ignored_name_patterns:
            files.append(entry.path)
    return files


async def run_on_start(module_name, file_path):
    """
    Start check of Worker
    :param module_name: worker module name
    :param file_path: path to worker
    :return:
    """
    absolute_path = os.path.abspath(file_path)
    sys.path.insert(0, absolute_path)
    module_spec = check_module(module_name)
    if module_spec:
        logger.info('Module spec exists')
        module = import_module_from_spec(module_spec)
    else:
        logger.info('Try import from source')
        import_source(module_name)

    logger.info(f'Start async: {module_name}')

    await module.main()


async def main():
    """
    Entrypoint
    :return:
    """
    files = scan_workers_folder()

    for file_path in files:
        module_name = os.path.basename(file_path).replace('.py', '')
        module_name = f'{Config().workers_up_name}{module_name}'
        logger.info(f'Detect worker: {module_name}')
        await run_on_start(module_name, file_path)

    logger.info('Start watch files...')

    async for changes in FileWatcher(Config().watch_dir):
        for ch in changes:
            change_type, file_path = list(ch)[0], list(ch)[1]
            filename = os.path.basename(file_path)
            absolute_path = os.path.abspath(file_path)
            module_name_from_file = filename.replace('.py', '')
            logger.info(f'File changed: {module_name_from_file}')
            sys.path.insert(0, absolute_path)

            module_name = f'{Config().workers_up_name}{module_name_from_file}'

            module_spec = check_module(module_name)
            if module_spec:
                logger.info('Module spec exists')
                module = import_module_from_spec(module_spec)
            else:
                logger.info('Try import from source')
                import_source(module_name)

            await module.main()


loop = asyncio.get_event_loop()
loop.run_until_complete(main())